package com.joyfulcorps.sneakycontroller;


import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.joyfulcorps.sneakycontroller.sneakycontroller.R;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MainActivity extends Activity implements Touchpad.TouchpadListener {

    private Touchpad touchpad;
    private TextView textView;
    private Socket socket;
    private String serverAddress;
    private String playerName;
    private boolean hasConnected;
    private long lastTouchEventTime;
    private static final int TOUCH_EVENT_COOLDOWN = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getActionBar();
        actionBar.hide();

        touchpad = (Touchpad)findViewById(R.id.touchpad);
        textView = (TextView)findViewById(R.id.textView);
        lastTouchEventTime = 0;
        touchpad.setListener(this);
        socket = null;
        initSocket();
    }

    @Override
    public void onBackPressed() {
        if(socket != null) {
            socket.off();
            socket.disconnect();
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        if (socket != null) {
            socket.off();
            socket.disconnect();
        }
        super.onDestroy();
    }

    private void initSocket() {
        final View cover = findViewById(R.id.cover);

        if (socket != null) {
            socket.off();
            socket.disconnect();
            hasConnected = false;
        }

        Intent intent = getIntent();
        playerName = intent.getStringExtra(PromptActivity.EXTRA_NICKNAME);
        serverAddress = intent.getStringExtra(PromptActivity.EXTRA_SERVER_ADDRESS);
        try {
            IO.Options opt = new IO.Options();
            opt.transports = new String[1];
            opt.transports[0] = "websocket";
            opt.reconnection = false;
            socket = IO.socket("http://" + serverAddress, opt);
            final Socket s = socket;
            s.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            cover.setVisibility(View.GONE);
                        }
                    });
                    hasConnected = true;
                    s.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                        @Override
                        public void call(Object... args) {
                            socket.connect();
                        }
                    });
                    s.emit("controller", playerName);
                }

            }).on(Socket.EVENT_ERROR, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if (!hasConnected) {
                        socket.off();
                        openPromptActivity("Can't connect to server!", Color.RED);
                    }
                    else {
                        socket.connect();
                    }
                }
            }).on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if (!hasConnected) {
                        socket.off();
                        openPromptActivity("Can't connect to server!", Color.RED);
                    }
                    else {
                        socket.connect();
                    }
                }
            }).on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if (!hasConnected) {
                        socket.off();
                        openPromptActivity("Connection timeout! Please retry", Color.RED);
                    }
                    else {
                        socket.connect();
                    }
                }
            });
            socket.connect();
        }
        catch (URISyntaxException e) {
            e.printStackTrace();
            Log.e("eh","something went wrong :(");
        }
    }

    private void openPromptActivity(String message) {
        Intent intent = new Intent(this, PromptActivity.class);
        intent.putExtra(PromptActivity.EXTRA_PROMPT_INFO, message);
        intent.putExtra(PromptActivity.EXTRA_PROMPT_INFO_COLOR, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void openPromptActivity(String message, int color) {
        Intent intent = new Intent(this, PromptActivity.class);
        intent.putExtra(PromptActivity.EXTRA_PROMPT_INFO, message);
        intent.putExtra(PromptActivity.EXTRA_PROMPT_INFO_COLOR, color);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void onJumpButtonClick(View b) {
        if (socket != null && socket.connected()) {
            socket.emit("j");
        }
    }

    public void onExplodeButtonClick(View b) {
        if (socket != null && socket.connected()) {
            socket.emit("e");
        }
    }

    public void onPowerupButtonClick(View b) {
        if (socket != null && socket.connected()) {
            socket.emit("p");
        }
    }

    @Override
    public void onTouchpadEvent(Touchpad.TouchpadEvent event) {
        if (socket != null && socket.connected()) {
            if (event.getType() == Touchpad.TouchpadEvent.TOUCH && event.getTime() - lastTouchEventTime > TOUCH_EVENT_COOLDOWN) {
                lastTouchEventTime = event.getTime();
                socket.emit("u", "{\"x\": " + event.getX() + ", \"y\": " + event.getY() + "}");
            }
            else if (event.getType() == Touchpad.TouchpadEvent.RELEASE) {
                socket.emit("u", "{\"x\": 0, \"y\": 0}");
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }



}
