package com.joyfulcorps.sneakycontroller;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.joyfulcorps.sneakycontroller.sneakycontroller.R;

import io.socket.client.Socket;

public class PromptActivity extends Activity {
    public final static String EXTRA_PROMPT_INFO = "com.sneakycontroller.joyfulcorps.EXTRA_PROMPT_INFO";
    public final static String EXTRA_PROMPT_INFO_COLOR = "com.sneakycontroller.joyfulcorps.EXTRA_PROMPT_INFO_COLOR";
    public final static String EXTRA_SERVER_ADDRESS = "com.sneakycontroller.joyfulcorps.EXTRA_SERVER_ADDRESS";
    public final static String EXTRA_NICKNAME = "com.sneakycontroller.joyfulcorps.EXTRA_NICKNAME";

    private Socket socket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prompt);
        try {
            Intent intent = getIntent();
            TextView info = (TextView)findViewById(R.id.prompt_info);
            String infoString = intent.getStringExtra(EXTRA_PROMPT_INFO);
            if (infoString.length() > 0) {
                info.setText(infoString);
                info.setTextColor(intent.getIntExtra(EXTRA_PROMPT_INFO_COLOR, Color.BLACK));
            }
        } catch (Exception ex) {}
    }

    public void connect(View v) {
        Intent intent = new Intent(this, com.joyfulcorps.sneakycontroller.MainActivity.class);
        intent.putExtra(EXTRA_SERVER_ADDRESS, ((EditText) findViewById(R.id.server_address_input)).getText().toString());
        intent.putExtra(EXTRA_NICKNAME, ((EditText) findViewById(R.id.nickname_input)).getText().toString());
        startActivity(intent);
    }
}
