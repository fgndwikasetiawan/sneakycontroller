package com.joyfulcorps.sneakycontroller;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joyfulcorps.sneakycontroller.sneakycontroller.R;

/**
 * Created by Wik on 06/09/2015.
 */
public class Touchpad extends RelativeLayout {

    public interface TouchpadListener {
        void onTouchpadEvent(TouchpadEvent e);
    }

    public class TouchpadEvent{
        private float x;
        private float y;
        private long time;
        private int type;
        public final static int TOUCH = 0;
        public final static int RELEASE = 1;

        TouchpadEvent(float x, float y, long time, int type) {
            this.x = x;
            this.y = y;
            this.time = time;
            this.type = type;
        }

        public float getX() {
            return x;
        }
        public float getY() {
            return y;
        }
        public long getTime() {
            return time;
        }
        public int getType() { return type; }
    }

    private ImageView circle;
    private TextView textView;
    private TouchpadListener listener;

    public Touchpad(Context context, AttributeSet attrs) {
        super(context, attrs);
        circle = (ImageView)findViewById(R.id.touchcircle);
        textView = (TextView)findViewById(R.id.textView);
        listener = null;
    }

    public void setListener(TouchpadListener l) {
        listener = l;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        circle = (ImageView)findViewById(R.id.touchcircle);
        textView = (TextView)findViewById(R.id.textView);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int circleHalfWidth = circle.getWidth() / 2;
        int circleHalfHeight = circle.getHeight() / 2;
        float maxX = (float)getWidth()/2 - (float)circleHalfWidth;
        float maxY = (float)getHeight()/2 - (float)circleHalfHeight;
        float x = event.getX(0);
        float y = event.getY(0);
        int action = event.getAction();
        if (action == event.ACTION_UP) {
            setCirclePosition(getWidth()/2 - (circle.getWidth() / 2) , getHeight()/2 - (circle.getWidth() / 2));
            listener.onTouchpadEvent(new TouchpadEvent(0, 0, event.getEventTime(), TouchpadEvent.RELEASE));
        }
        else if (action == event.ACTION_DOWN || action == event.ACTION_MOVE) {
            int halfWidth = circle.getWidth()/2;
            int halfHeight = circle.getHeight()/2;
            x = x > halfWidth ? x : halfWidth;
            y = y > halfHeight ? y : halfHeight;
            x = x < getWidth() - halfWidth ? x : getWidth() - circle.getWidth()/2;
            y = y < getHeight() - halfHeight ? y : getHeight() - circle.getHeight() / 2;
            setCirclePosition((int) (x - halfWidth), (int) (y - halfHeight));
            listener.onTouchpadEvent(new TouchpadEvent( (x - (float)getWidth()/2) / maxX, (y - (float)getHeight()/2) / maxY, event.getEventTime(), TouchpadEvent.TOUCH));
        }
        return true;
    }

    private void setCirclePosition(int left, int top) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)circle.getLayoutParams();
        params.addRule(RelativeLayout.CENTER_HORIZONTAL, 0);
        params.addRule(RelativeLayout.CENTER_VERTICAL, 0);
        params.leftMargin = left;
        params.topMargin = top;
        circle.setLayoutParams(params);
    }

}
